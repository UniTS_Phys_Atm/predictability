#!/usr/bin/bash
#
# This program sbatches run_wrf_A2_4_emaiero.job asking for 1 node.
#
# Author: Enrico Maiero    
# e-mail: enricomaiero@virgilio.it
# University of Trieste
# Departmente of Physics
# ITALY
#
# Last update: Dic 09, 2018
#
# -----------------------------------------------------------------------------
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   | GENERAL SETTINGS AND DEBUG OPTIONS     | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
   #set -x # Let script verbose commanda (activate it for debugs)
   set -e # Let script exit if a command fails
   set -u # Let script exit if an unset variable is used
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   | CONSTANT AND PARAMETERS HERE BELOW     | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
   #EXIT_STATUS=0          # Default exit status. 0 = everything was OK
   SOUJOB="/marconi_work/uTS18_Giaiotti/predictability/job/run_wrf_A2_4_emaiero_npc.job.tpl"
   OUTDATA="/marconi_work/uTS18_Giaiotti/emaiero0/scalabilita/identifica.dat"
   NCOMPPERNOD=21
   NNODI=18
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   |         MAIN SCRIPT HERE BELOW         | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
   for INDICE in $(seq 1 1 ${NNODI}); do
       NCOMPITI=$((${INDICE}*${NCOMPPERNOD}))
       ID=$( sed "s/%%NODI%%/${INDICE}/; s/%%COMPITI%%/${NCOMPITI}/; s/%%COMPPERNOD%%/${NCOMPPERNOD}/" "${SOUJOB}" | sbatch)
       #ID="1 $INDICE" #controllo, da commentare
       ID="${ID##* }"
       VUE=$(date)
       echo -e "${ID} ${VUE} ${INDICE} ${NCOMPITI} ${NCOMPPERNOD}" >> ${OUTDATA}
   done
   #some verbose before to exit
   echo -e "dut a puest"
