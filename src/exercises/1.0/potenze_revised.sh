#!/usr/bin/bash
#
# This program calculates powers of 2 with integer exponent going from 0 to an 
# input final value given from the user.
# The program exploits the definition of power as recursive multiplication, 
# paying attention to the meaning of 2**0.
#
# Author: Enrico Maiero    
# e-mail: enricomaiero@virgilio.it
# University of Trieste
# Departmente of Physics
# ITALY
#
# Last update: Oct 22, 2018
#
# -----------------------------------------------------------------------------
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   | GENERAL SETTINGS AND DEBUG OPTIONS     | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
   #set -x # Let script verbose commanda (activate it for debugs)
   set -e # Let script exit if a command fails
   set -u # Let script exit if an unset variable is used
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   | CONSTANT AND PARAMETERS HERE BELOW     | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
   EXIT_STATUS=0          # Default exit status. 0 = everything was OK
   readonly BASE=2        # Base of the power
   FOUT="powers_of_2.txt" # Output file name 

   EXP=0  # Initialize the exponent
   POT=1  # Initialize the first element of the series
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   |         MAIN SCRIPT HERE BELOW         | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
   #   This is the input section. Exponent for BASE is loaded 
   #   from the stdin
   #
   echo -e "\n\tThis program calculates powers of 2 with integer exponent going from "
   echo -e "\t0 to an input final value given from the user."
   echo -e "\n\tGive me the maximum value for the exponent:"
   read -r EXPM  # Reading isntruction
   readonly EXPM=$EXPM #I am ensuring that EXPM will not change while the program is running
   #
   #Create the output file
   echo "Powers of $BASE for EXPonent going from 0 to $EXPM" > $FOUT
   echo "BASE, EXPonent, power" > $FOUT
   #
   #Loop, condition: is the EXPonent less or equal of EXPM?"
   while [[ $EXP -le $EXPM ]]; do
         #Calculation performed only for EXP not 0
         if [[ $EXP -ne 0 ]]; then
            OK=0
            POT=$((POT*BASE)) || OK=1
            if [[ $OK -eq 1 ]];then 
               echo -e "\n\tERROR .... bla bla\n"
               EXIT_STATUS=1; exit $EXIT_STATUS 
            fi
         fi
         echo "$BASE,$EXP,$POT" >> $FOUT
         #Counter incremented
         EXP=$((EXP+1))
   done
   #
   # Some verbose before to leave
   #
   echo -e "\n\tEverything went OK.\n\tOutput file is: $FOUT\n\tbye\n"
   #
   exit $EXIT_STATUS 
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   |           END OF THE SCRIPT            | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
