#!/usr/bin/bash
#
# This program calculates the first N terms of an exponential sequence of
# the number of Nepero e, given the minimum and maximum exponents.
# These two as well as N are parameters given by the user as respectively
# the second, third and first argument of the script. 
# 
# Author: Enrico Maiero    
# e-mail: enricomaiero@virgilio.it
# University of Trieste
# Departmente of Physics
# ITALY
#
# Last update: Nov 06, 2018
#
# -----------------------------------------------------------------------------
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   | GENERAL SETTINGS AND DEBUG OPTIONS     | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
   #set -x # Let script verbose commanda (activate it for debugs)
   set -e # Let script exit if a command fails
   set -u # Let script exit if an unset variable is used
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   | CONSTANT AND PARAMETERS HERE BELOW     | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
   EXIT_STATUS=0          # Default exit status. 0 = everything was OK
   SOLO=0 #control variable used to write the warning message in the output file
   #
   #scale for the real numbers
   SCA=20
   # Ensuring that the first three arguments of the script exist and are not empty
   # If so, assigning to the proper variables their values
   if  [[ $# -gt 0 ]] &&  [[  ! -z $1 ]] && [[ ! -z $2 ]] && [[ ! -z $3 ]];then 
      readonly N=$1
      readonly MINE=$2
      readonly MAXE=$3
   else
      echo -e "\n\tError! Missing argument. I must exit!\n"
      EXIT_STATUS=1; exit $EXIT_STATUS
   fi
   readonly SEP=$( echo -e "scale=$SCA; ( $MAXE - $MINE ) / ($N-1) " | bc )
   # echo -e $SEP
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   |         MAIN SCRIPT HERE BELOW         | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
   echo -e "\n\tThis program calculates $N terms of an exponential sequence"
   echo -e "\tfor exponents between $MINE and $MAXE. The base is the Nepero num."
   #
   #Initializing the variables
   I=0
   #
   #Creating the output file
   FOUT="output_${N}_${MINE}_${MAXE}.txt"      # Output file name
   echo -e "#Here there are $N terms of an exponential sequence" > $FOUT
   echo -e "#for exponents between $MINE and $MAXE. The base is the Nepero num." >> $FOUT
   echo -e "#index, exponent, exponential" >> $FOUT
   #
   #Loop, condition: is the index less than N?"
   while [[ $I -lt $N ]]; do
         OK=0
         EXP=$( echo -e "scale=$SCA; $MINE + $I * $SEP " | bc ) || OK=1
         # echo -e $EXP #print di controllo
         POT=$( echo -e "scale=$SCA; e($EXP) " | bc -l) || OK=1
         # echo -e $POT #print di controllo
         if [[ $OK -eq 1 ]];then 
               echo -e "\n\tERROR: something went wrong in the loop\n"
               EXIT_STATUS=1; exit $EXIT_STATUS 
         fi
         echo -e "$I,$EXP,$POT" >> $FOUT
         #Counter incremented
         I=$(($I+1))
         #echo -e $I #print di controllo
   done
   #
   # Some verbose before to leave
   #
   echo -e "\n\tEverything went OK.\n\tOutput file is: $FOUT\n\tbye\n"
   #
   exit $EXIT_STATUS 
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   |           END OF THE SCRIPT            | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
