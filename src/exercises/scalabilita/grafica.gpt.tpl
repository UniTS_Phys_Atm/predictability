# This script fits the files of the type graf_piunodi.txt.
#
set term 'postscript'
set output '%%NOMEIMMAGINE%%.ps'
set title 'scalability curve'
set xlabel 'number of tasks'
set ylabel 'time[sec]'
p '%%TIPO1%%' u 1:2 t '%%DIDASCATIPO1%%', '%%TIPO2%%' u 1:2 t '%%DIDASCATIPO2%%', '%%TIPO3%%' u 1:2 t '%%DIDASCATIPO3%%'
