#!/usr/bin/bash
#
# This program calculates the first N terms of the Fibonacci sequence
# using bc.
# N is a parameter given by the user as the first argument of the script. 
# 
# Author: Enrico Maiero    
# e-mail: enricomaiero@virgilio.it
# University of Trieste
# Departmente of Physics
# ITALY
#
# Last update: Nov 21, 2018
#
# -----------------------------------------------------------------------------
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   | GENERAL SETTINGS AND DEBUG OPTIONS     | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
   #set -x # Let script verbose commanda (activate it for debugs)
   set -e # Let script exit if a command fails
   set -u # Let script exit if an unset variable is used
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   | CONSTANT AND PARAMETERS HERE BELOW     | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
   EXIT_STATUS=0          # Default exit status. 0 = everything was OK
   readonly MAXINT=$((10**18)) #near the maximum representable integer
   SOLO=0 #control variable used to write the warning message in the output file
   #echo $MAXINT #print di controllo 
   #
   # Ensuring that the first argument of the script exists and is not empty
   # If so, assigning to the variable FIN the name of th input file
   if  [[ $# -gt 0 ]] &&  [[  ! -z $1 ]];then 
      readonly N=$1
   else
      echo -e "\n\tError! Input maximum index not given. I must exit!\n"
      EXIT_STATUS=1; exit $EXIT_STATUS
   fi
   #scale for the real numbers
   SCA=2
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   |         MAIN SCRIPT HERE BELOW         | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
   #   This is the input section.
   #
   echo -e "\n\tThis program calculates the first $N terms of the Fibonacci sequence."
   #
   #Initializing the variables
   F1=1
   F2=1
   I=3
   #
   #Creating the output file
   FOUT="fibonacci_${N}.txt"      # Output file name
   echo -e "#Here there are the first $N terms of the Fibonacci sequence " > $FOUT
   echo -e "#index, term" >> $FOUT
   echo -e "1,$F1" >> $FOUT
   echo -e "2,$F2" >> $FOUT
   #
   #Loop, condition: is the index less or equal to N?"
   while [[ $I -le $N ]]; do
         OK=0
         A=$F2 || OK=1
         F2=$( echo -e "scale=$SCA; $F1 + $F2" | bc) || OK=1
         F1=$A || OK=1
         if [[ $OK -eq 1 ]];then 
               echo -e "\n\tERROR: something went wrong in the loop\n"
               EXIT_STATUS=1; exit $EXIT_STATUS 
         fi
         #warning in case of overflow
         if [[ $F2 -ge $MAXINT ]]; then
           # echo $SOLO #print di controllo
             if [[ $SOLO -eq 0 ]]; then #Ensuring that the message will appear only once
               echo "#from now on the terms are exceedding 10**18" >> $FOUT
               SOLO=1
             fi
         fi
         echo -e "$I,$F2" >> $FOUT
         #Counter incremented
         I=$(($I+1))
         #echo -e $I #print di controllo
   done
   #
   # Some verbose before to leave
   #
   echo -e "\n\tEverything went OK.\n\tOutput file is: $FOUT\n\tbye\n"
   #
   exit $EXIT_STATUS 
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   |           END OF THE SCRIPT            | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
