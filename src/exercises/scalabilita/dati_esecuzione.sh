#!/usr/bin/bash
#
# This program extracts from the output of the command sacct some useful 
# data and save them in a file. Then it writes another file readable by
# gnuplot. The job numbers are extracted from identifica.dat 
# 
# Author: Enrico Maiero    
# e-mail: enricomaiero@virgilio.it
# University of Trieste
# 
# ITALY
#
# Last update: Dic 08, 2018
#
# -----------------------------------------------------------------------------
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   | GENERAL SETTINGS AND DEBUG OPTIONS     | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
   #set -x # Let script verbose commanda (activate it for debugs)
   set -e # Let script exit if a command fails
   #set -u # Let script exit if an unset variable is used
   #    
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   | CONSTANT AND PARAMETERS HERE BELOW     | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
   EXIT_STATUS=0          # Default exit status. 0 = everything was OK
#   OK=0   #control variable
#   SALTA=0  #another control variable
   #Extracting JobID from squeue: NOT USEFUL
#   NUMERO=$(squeue --format=%i -u $USER | sed /JOBID/d)
   #Working file
   WORK1="dati_1nodo.txt"
   WORK2="dati_piunodi.txt"
   WORK3="dati_npc.txt"
   #Working directory
   readonly WORSOU="/marconi_work/uTS18_Giaiotti/emaiero0/scalabilita/identifica.dat"
   #output file
   FOUT1="graf_1nodo.txt"
   FOUT2="graf_piunodi.txt"
   FOUT3="graf_npc.txt"
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   |         MAIN SCRIPT HERE BELOW         | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
   echo -e "#Ntasks,Elapsed,JobID" >> ${FOUT1}
   echo -e "#Ntasks,Elapsed,JobID" >> ${FOUT2}
   echo -e "#Ntasks,Elapsed,JobID" >> ${FOUT3}
   #reading job numbers from identifica.dat and extracting fields we are interested in
   while read line; do
#     echo -e $SALTA #control echo
#     echo -e $line #control echo
     if [[ SALTA -eq 0 ]]; then #ensuring that the first line of identifica.dat is skipped
       SALTA=1
     else
       ARRA=($line)
       JOBNUM=${ARRA[0]}
       TUTTO=$( sacct -p -l -j ${JOBNUM} | sed /Elapsed/d)
 #      echo -e ${JOBNUM} #control echo
 #      echo -e ${ARRA[7]} #control echo
       FIELDS=($(echo -e ${TUTTO} | sed "s/|||/|noval|noval|/g; s/||/|noval|/g; s/|/ /g" ))
       NCOMPITI=${ARRA[8]} #${FIELDS[20]} #this would read ntasks fro sacct, but the field is empty if read from the JobID without extension
       TEMPACCIO=${FIELDS[22]}
       TEMPI=($(echo -e ${TEMPACCIO} | sed "s/:/ /g" ))
#       echo -e $TEMPACCIO #control echo
#       echo -e ${TEMPI[*]} #control echo
       WALLTIME=$( echo -e "3600*${TEMPI[0]} + 60*${TEMPI[1]} + ${TEMPI[2]}" | bc)
          if [[ ${ARRA[7]} -eq 1 ]]; then
             echo -e ${TUTTO} >> ${WORK1}
             echo -e "${NCOMPITI} ${WALLTIME} ${JOBNUM}" >> ${FOUT1}
          else
                if [[ ! -z "${ARRA[9]}" ]]; then
#                     echo -e ${ARRA[9]} #control echo
                     echo -e ${TUTTO} >> ${WORK3}
                     echo -e "${NCOMPITI} ${WALLTIME} ${JOBNUM}" >> ${FOUT3}
                else
                     echo -e ${TUTTO} >> ${WORK2}
                     echo -e "${NCOMPITI} ${WALLTIME} ${JOBNUM}" >> ${FOUT2}
                fi
          fi
     fi
   done < ${WORSOU}
#omitted because only the first line of sacct is read
   #deleting from the final files all lines concerning with .bat+ and .ext+
#   sed "/.bat+/d; /.ext+/d" ${FOUT1} #> ${FOUT1}
#   sed "/.bat+/d; /.ext+/d" ${FOUT2} #> ${FOUT2}
#   sed "/.bat+/d; /.ext+/d" ${FOUT3} #> ${FOUT3}
   #Control sequence
   if [[ $OK -eq 1 ]];then 
               echo -e "\n\tERROR: something went wrong\n"
               EXIT_STATUS=1; exit $EXIT_STATUS 
   fi
  #some verbose before to leave
  #
#  rm $WORKING
  echo -e "\n\tEverything went OK.\n\tOutput files are: ${WORK1}, ${WORK2}, ${WORK3}, ${FOUT1}, ${FOUT2}, ${FOUT3}\n\tbye\n"
  #
  exit $EXIT_STATUS 
  #    
  #   +----------------------------------------+  
  #   |                                        | 
  #   |           END OF THE SCRIPT            | 
  #   |                                        | 
  #   +----------------------------------------+  
  #

