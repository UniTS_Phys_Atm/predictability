#!/usr/bin/bash
#
# This script calls the script ../1.1/potenze_new.sh.
# A list of the input files present in a directory indicated by the user
# is written into the document list.txt. The script then reads each line
# of list.txt and uses each line as the name of an input file. When the eof
# is reached, the run halts.
#
# Author: Enrico Maiero    
# e-mail: enricomaiero@virgilio.it
# University of Trieste
# Departmente of Physics
# ITALY
#
# Last update: Oct 31, 2018
#
# -----------------------------------------------------------------------------
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   | GENERAL SETTINGS AND DEBUG OPTIONS     | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
   #set -x # Let script verbose commanda (activate it for debugs)
   set -e # Let script exit if a command fails
   set -u # Let script exit if an unset variable is used
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   | CONSTANT AND PARAMETERS HERE BELOW     | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
   EXIT_STATUS=0          # Default exit status. 0 = everything was OK
   #    
   # Ensuring that the first argument of the script exists and is not empty
   # If so, assigning to the variable DIR the name of the directory containing
   # the input files
   if  [[ $# -gt 0 ]] &&  [[  ! -z $1 ]];then
      readonly DIR=$1
   else
      echo -e "\n\tError! Source directory not given. I must exit!\n"
      EXIT_STATUS=1; exit $EXIT_STATUS
   fi
   # working direcotry:
   readonly WORK=$(pwd)
   #
   #   +----------------------------------------+  
   #   |                                        | 
   #   |         MAIN SCRIPT HERE BELOW         | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
   echo -e
   echo -e "This program calls the script ../1.1/potenze_new.sh and runs it "
   echo -e "for each of the input files present in the directory you gave me  "
   echo -e "tas argument of the script. "
   echo -e
   # Writing the input files in list.txt. The program recognizes the input file
   # through the extension .ini. In case such a file were wrongly written, the program
   # would try to use an unset variable and this would result in an error.
   cd $DIR
   ls -1 $PWD/*.ini > "$WORK/list.txt"
   echo -e "#EOF" >> "$WORK/list.txt"
   cd $WORK
   #
   # Counting the number of lines of list.txt and using this to verify that at least
   # an input file exists.
   STRING=$(wc -l list.txt)
   set -- $STRING
   readonly N=$(($1-1))
   # LE SEGUENTI 4 LINEE SONO STATE RIMOSSE PERCHE RIDONDANTI
   # if [[ $N = 0 ]]; then
   #            echo -e "\n\tERROR: there are no input files in $DIR\n"
   #            EXIT_STATUS=1; exit $EXIT_STATUS
   # fi
   # 
   # Here comes the loop
   for I in $(seq 1 1 $N); do
         IFS='' read -r NOMEFIN 
         echo -e "$I -th input file: $NOMEFIN"
         # NOMEFIN="$DIR/$NOMEFIN" #RIMOSSO DOPO UTILIZZO DI $PWD/*
         #calling the elder script with argument the current input file
         #note: it is up to the elder script to verify the existence of the input file
         bash $HOME/src/predictability/src/exercises/1.1/potenze_new.sh $NOMEFIN     
   done < list.txt
   #
   # Some verbose before to leave
   #
   echo -e "Everything went OK. $N output file have been generated. "
   #
   exit $EXIT_STATUS 
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   |           END OF THE SCRIPT            | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
