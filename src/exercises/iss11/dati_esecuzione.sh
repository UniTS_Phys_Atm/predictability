#!/usr/bin/bash
#
# This program extracts from the output of the command sacct some useful 
# data and save them in a file readable by gnuplot.
# 
# Author: Enrico Maiero    
# e-mail: enricomaiero@virgilio.it
# University of Trieste
# 
# ITALY
#
# Last update: Nov 27, 2018
#
# -----------------------------------------------------------------------------
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   | GENERAL SETTINGS AND DEBUG OPTIONS     | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
   #set -x # Let script verbose commanda (activate it for debugs)
   set -e # Let script exit if a command fails
   set -u # Let script exit if an unset variable is used
   #    
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   | CONSTANT AND PARAMETERS HERE BELOW     | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
   EXIT_STATUS=0          # Default exit status. 0 = everything was OK
   OK=0   #control variable
   #Extracting JobID from squeue: NOT USEFUL
#   NUMERO=$(squeue --format=%i -u $USER | sed /JOBID/d)
   #Working file
   WORKING="intermedio.txt"
   #Working directory
   WORDIR=/marconi_scratch/userexternal/emaiero0/issue_11
   #output file
   FOUT="dati_esecuzione.txt"
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   |         MAIN SCRIPT HERE BELOW         | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
   echo -e "#JobID,JobName,User,Partition,AllocNodes,AllocCPUs,NNodes,Ntasks,SystemCPU,TotalCPU,AveCPU,CPUTime,CPUTimeRAW,Elapsed,AveVMSize,Maxindex" >> $FOUT
   #Extracting fields we are interested in
   sacct -p --format=JobID,JobName,User,Partition,AllocNodes,AllocCPUs,NNodes,Ntasks,SystemCPU,TotalCPU,AveCPU,CPUTime,CPUTimeRAW,Elapsed,AveVMSize |\
 sed -e "/JobID/d; /^--/d; s/|||/|noval|noval|/g; s/||/|noval|/g; s/00*/0/g; /batch/d; /extern/d" > $WORKING || OK=1 #saving output in a working file eith tabs as separators
#   sacct -p -l -j | | cut -d "|" -f 1,3,,4,,22,,21,,,20,,23,8 | sed -e s/JobID/#JobID/g >> $WORKING || OK=1
   #translating time in seconds; cases due to the presence of different tipes of format
   #moreover looking for lengths of fibonacci sequences and writing all results in the output file.
   while read line; do
     echo $line
     FIELDS=($(echo "$line" | tr '|' ' '))  #saving each type of data of a specific set in an array
     DATFILE="${WORDIR}/$(ls $WORDIR | grep ${FIELDS[0]}.out ||echo -e  "nie di fa")"  #looking for lengths of fibonacci sequences.
     read -d "|" MAXIN < $DATFILE || echo -e  "nie di fa"  #looking for lengths of fibonacci sequences.
     for INDEX in 8 9; do
        TEMPO=($(echo -e "${FIELDS[${INDEX}]}" | tr ':' ' '))
        OTTO=$( echo -e "${TEMPO[0]} * 60 + ${TEMPO[1]}" | bc || echo -e "noval" )
        FIELDS[${INDEX}]=$OTTO
     done
     for INDEX in 10 11 13; do
        TEMPO=($(echo -e "${FIELDS[${INDEX}]}" | tr ':' ' '))
        OTTO=$( echo -e "${TEMPO[0]} * 3600 + ${TEMPO[1]} * 60 + ${TEMPO[2]}" | bc )
        FIELDS[${INDEX}]=$OTTO
     done
     echo -e "$(echo -e "${FIELDS[@]}" | tr ' ' "\t\t")\t\t${MAXIN}" >> $FOUT
   done < $WORKING 
   #Control sequence
   if [[ $OK -eq 1 ]];then 
               echo -e "\n\tERROR: something went wrong\n"
               EXIT_STATUS=1; exit $EXIT_STATUS 
   fi
  #some verbose before to leave
  #
#  rm $WORKING
  echo -e "\n\tEverything went OK.\n\tOutput file is: $FOUT\n\tbye\n"
  #
  exit $EXIT_STATUS 
  #    
  #   +----------------------------------------+  
  #   |                                        | 
  #   |           END OF THE SCRIPT            | 
  #   |                                        | 
  #   +----------------------------------------+  
  #

