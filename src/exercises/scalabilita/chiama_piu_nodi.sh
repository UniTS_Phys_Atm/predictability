#!/usr/bin/bash
#
# This program sbatches run_wrf_A2_4_emaiero.job asking for 1 node.
#
# Author: Enrico Maiero    
# e-mail: enricomaiero@virgilio.it
# University of Trieste
# Departmente of Physics
# ITALY
#
# Last update: Dic 09, 2018
#
# -----------------------------------------------------------------------------
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   | GENERAL SETTINGS AND DEBUG OPTIONS     | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
   #set -x # Let script verbose commanda (activate it for debugs)
   set -e # Let script exit if a command fails
   set -u # Let script exit if an unset variable is used
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   | CONSTANT AND PARAMETERS HERE BELOW     | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
   #EXIT_STATUS=0          # Default exit status. 0 = everything was OK
   SOUJOB="/marconi_work/uTS18_Giaiotti/predictability/job/run_wrf_A2_4_emaiero.job.tpl"
   OUTDATA="/marconi_work/uTS18_Giaiotti/emaiero0/scalabilita/identifica.dat"
   NCOMPITI="56 69 76 85 105 125 145 165 185 210 230 250 260 270 290 310 330"
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   |         MAIN SCRIPT HERE BELOW         | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
   for INDICE in ${NCOMPITI}; do
       NNODI=$((${INDICE}/68))
       if [[ $((${INDICE}%68)) -ne 0 ]]; then
         NNODI=$((${NNODI}+1))
       fi
       ID=$( sed "s/%%NODI%%/${NNODI}/; s/%%COMPITI%%/${INDICE}/" "${SOUJOB}" | sbatch)
       #ID="1 $INDICE" #controllo, da commentare
       ID="${ID##* }"
       VUE=$(date)
       echo -e "${ID} ${VUE} ${NNODI} ${INDICE}" >> ${OUTDATA}
   done
   #some verbose before to exit
   echo -e "dut a puest"
