#!/bin/bash
#
# BASH script to donwnload and archive the EURO-CORDEX climate
# projection from OSMER web site
#
# by Dario B. Giaiotti
# on February 26, 2020
#
# set -x
  set -e
  set -u
#
# Constants and parameters here below
#
# Set of available fields
#
#    n, field, extened name Eng, extended name Ita, [units]
#  - 01 evspsbl "Evaporation" Evaporazione [kg m-2 s-1]
#  - 02 hfls "Surface Upward Latent Heat Flux" Flusso superficiale di calore latente [W m-2] 
#  - 03 hfss "Surface Upward Sensible Heat Flux" Flusso superficiale di calore sensibile [W m-2]
#  - 04 huss "Near-Surface Specific Humidity" Umidita` specifica superficiale [1] 
#  - 05 mrro "Total Runoff" Runoff totale [kg m-2 s-1]
#  - 06 pr "Precipitation" Precipitazioni [kg m-2 s-1]
#  - 07 ps "Surface Air Pressure" Pressione superficiale dell'aria [Pa]
#  - 08 rlds "Surface Downwelling Longwave Radiation" Radiazione superficiale di onde lunghe [W m-2]
#  - 09 rsds "Surface Downwelling Shortwave Radiation" Radiazione superficiale di onde corte [W m-2]
#  - 10 snw "Surface Snow Amount" Accumulo di neve superficiale [kg m-2]
#  - 11 tas "Near-Surface Air Temperature" Temperatura dell'aria vicino al suolo [K]
#  - 12 tasmax "Daily Maximum Near-Surface Air Temperature" Temperatura massima dell'aria vicino al suolo [K]
#  - 13 tasmin "Daily Minimum Near-Surface Air Temperature" Temperatura minima dell'aria vicino al suolo [K] 
#  - 14 ts "Surface Temperature" Temperatura al suolo [K]
#  - 15 uas "Eastward Near- Surface Wind Velocity" Vento da est vicino al suolo [m s-1]
#  - 16 vas "Northward Near- Surface Wind Velocity" Vento da nord vicino al suolo [m s-1]
#
# Set of models to be downloaded  
  LISTA="05_MPI-ESM-LR_REMO2009  04_HadGEM2-ES_RACMO22E  03_EC-EARTH_RCA4  02_EC-EARTH_RACMO22E 01_EC-EARTH_CCLM4-8-17"
#
# Set of fields to be downloaded  
  VARS="01_evspsbl 02_hfls 03_hfss 04_huss 05_mrro 06_pr 07_ps 08_rlds 09_rsds 10_snw 11_tas 12_tasmax 13_tasmin 14_ts 15_uas 16_vas"
#
# Web host 
#
  WEB_HOST="https://www.meteo.fvg.it/clima/clima_fvg/03_cambiamenti_climatici/03_MODELLI_CLIMATICI_per_il_FVG"
#
# Root dir for data archive
#
  ROOT_DATA="$WORK/dgiaiott/EURO_CORDEX_on_FVG"
#
# Move in the working directory for download
#
  cd $ROOT_DATA || exit 1
#
# Download zip files looping over models and selected fields
#
if [[ 0 -eq 1 ]];then
 for D in $LISTA; do 
     for V in $VARS;do
         DATA_DIR="${ROOT_DATA}/${D}/${V}"
         echo -e "\n\tGetting from model: ${D} --> field: ${V}"
         wget ${WEB_HOST}/${D}/${V}/${V}.zip   # Get zip file
         if [[ ! -d ${DATA_DIR} ]];then mkdir -p $DATA_DIR || exit 2; fi
         mv -v ${V}.zip ${DATA_DIR}            # move zip file
     done
 done
fi
#
# unzip downloaded files 
#
 for D in $LISTA; do 
     for V in $VARS;do
         DATA_DIR="${ROOT_DATA}/${D}/${V}"
         if [[ -d ${DATA_DIR} ]];then cd $DATA_DIR || exit 2; fi
         echo -e "\n\tDecompression of: ${D} --> field: ${V}"
         if [[ -e ${V}.zip ]];then 
            unzip -v ${V}.zip ||  echo -e "\n\n\tIMPOSSIBLE TO DECOMPRESS: ${DATA_DIR}/{V}.zip"
            unzip -o ${V}.zip || echo -e "\tIMPOSSIBLE TO DECOMPRESS: ${DATA_DIR}/{V}.zip\n\n"
            rm -v ${V}.zip
            chmod -v 0444 *.nc
         else
            ls -l | sed s/^/\\t/g
         fi
     done
 done
#
# Check disk usage  
#
  echo -e "\n\n\tCheck disk space used: ${ROOT_DATA}"
  du -shc ${ROOT_DATA}/*
#
# Exit from script
#
 exit 0

#         wget ${WEB_HOST}/${D}/14_ts/14_ts.zip   # Get zip file

#         wget ${WEB_HOST}/${D}/14_ts/14_ts.zip   # Get zip file
#         mv -v 14_ts.zip ${D}-14_ts.zip        # move zip file
