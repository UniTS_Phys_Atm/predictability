#! bin/bash
# This program calculates powers of 2 with integer exponent going from 0 to an input final value given from the user.
# The program exploits the definition of powe as recursive multiplication, paing attention to the meaning of 2**0.
# Author: Enrico Maiero    Address: enricomaiero@virgilio.it

#Let script exit if a command fails
set -e
#Let script exit if an unset variable is used
set -u

#Define or read the parameters
readonly base=2
echo "This program calculates powers of 2 with integer exponent going from 0 to an input final value given from the user."
echo "Give me the maximum value for the exponent:"
read -r expm
readonly expm=$expm #I am ensuring that expm will not change while the program is running
# echo $expm #print di controllo

#Initialize the variables
exp=0
pot=1

#Create the output file
echo "Powers of $base for exponent going from 0 to $expm" > powers_of_2.txt
echo "base, exponent, power" > powers_of_2.txt

#Loop, condition: is the exponent less or equal of expm?"
while [ $exp -le $expm ]; do
  #Calculation performed only for exp not 0
  if [ $exp -ne 0 ]; then
   pot=$((pot*base))
   # echo $pot #print di controllo
  fi
  echo "$base,$exp,$pot" >> powers_of_2.txt
  #Counter incremented
  exp=$((exp+1))
  # echo $exp #print di controllo
done


