#!/usr/bin/bash
#
# This script generates a set of similar files which are supposed to be 
# input files of the scripts present in the directory predictability/src/exercises
# such as potenze_new.sh. The number of such files is decided by the users,
# as much as the directory where the files will be put. Both these parameters
# are to be given as arguments of the script.
# 
# Author: Enrico Maiero    
# e-mail: enricomaiero@virgilio.it
# University of Trieste
# Departmente of Physics
# ITALY
#
# Last update: Oct 31, 2018
#
# -----------------------------------------------------------------------------
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   | GENERAL SETTINGS AND DEBUG OPTIONS     | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
   #set -x # Let script verbose commanda (activate it for debugs)
   set -e # Let script exit if a command fails
   set -u # Let script exit if an unset variable is used
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   | CONSTANT AND PARAMETERS HERE BELOW     | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
   EXIT_STATUS=0          # Default exit status. 0 = everything was OK
   #    
   # Ensuring that the first and second arguments of the script exists and is not
   # empty. If so, assigning to the variable DIR the name of the directory containing
   # the input files and to the variable N the number of the files that are to be
   # generated.
   if  [[ $# -gt 0 ]] &&  [[  ! -z $1 ]] && [[  ! -z $2 ]];then
     readonly  DIR=$1
     readonly  N=$2
   else
      echo -e "\n\tError! Source directory not given. I must exit!\n"
      EXIT_STATUS=1; exit $EXIT_STATUS
   fi
   #
   #   +----------------------------------------+  
   #   |                                        | 
   #   |         MAIN SCRIPT HERE BELOW         | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
   echo -e
   echo -e "\n\tThis program creates $N input files for the scripts present "
   echo -e "\tin the directory predictability/src/exercises such as potenze_new.sh."
   echo -e "\n\tThe input file will present the following strcture: "
   echo -e "\n\t\t readonly BASE=<value of the base> "
   echo -e "\n\t\t readonly EXPM=<value of the maximum exponent>"
   echo -e "\n\tThe generated files will be found in the directory"
   echo -e "\t$DIR"
   echo -e
   M=1; 
   for B in 2 3 4 5 6 7 10 12 15 50 100; do 
         for I in $(seq 0 1 $N);do
			   SUFFISSO="_"
               SUFFISSO="$B$SUFFISSO$I"
               cat << EOF > $DIR/file_input_$SUFFISSO.ini
               # This is a data file loadable by the scripts potenze_base_libera.sh,
               # potenze_base_libera_avv.sh, potnze_new.sh.
               # Here we select the values of the base and the maximum exponent.
               readonly BASE=$B 
               readonly EXPM=$I 
EOF
               M=$(($M+1)) 
         done 
   done
   #
   # Some verbose before to leave
   #
   echo -e "\n\tEverything went OK."
   echo -e "\n\t$M files have been created in the directory $DIR\n\tbye\n"
   #
   exit $EXIT_STATUS 
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   |           END OF THE SCRIPT            | 
   #   |                                        | 
   #   +----------------------------------------+  
   #

