#
# This is the WRF real case simulation initialization file template
# A simulaztion is defined by means of the variables and parameters 
# filled in here below.
#
# THESE SETTINGS ARE FOR THE STUDENT ENRICO MAIERO PURPOSES
# IN THE FRAME OF HIGH PERFORMANCE COMPUTING STAGE
# Nov 18, 2018
#
# This initialization file is included in the job scripts, at beginnig
# and variables are used to characterize the simulation. It is loaded
# by means of the source BASH command
#
# Job scripts requiring this initialziation file are:
#
#     run_geogrid.job  to run the geogrid.exe and produce static boundary conditions
#     run_ungrib.job   to run the ungrib.exe  and prepare the files for the initial  
#                      conditions and the dynamic boundary conditions
#
#     run_metgrid.job  to run the metgrid.exe  and produce the initial and the dynamic
#                      boundary conditions
#     run_real.job     to run the real.exe  and produce the interpolated file of initial
#                      and boundary conditions
#     run_wrf.job      to run the wrf.exe  and produce the Rsimulaztion outputs
#
# Variables and parameters are grouped in sections, each one is specific for the job
# to be submitted to the queue. Furthemore there is a general section which referes to 
# the variables shared among all the jobs.
#
# By Dario B. Giaiotti
# Dep. of Physics
# University of Trieste
# Physics of the Atmosphere 
# Atmospheric boundary Layer
#
# ==================================================================================
#
# +--------------------------------------------------------------------------------+
# |                                                                                |
# |                      GENERAL VARIABLES AND PARAMETERS                          |
# |                                                                                |
# +--------------------------------------------------------------------------------+
#
#
  SIMULATION="Ophelia_TC_4EM"                                     # Simulation identification code 
  DS="2017101200"                                                 # Simulation start date YYYYMMDDHH
  DE="2017101206"                                                 # Simulation end   date YYYYMMDDHH
  HOME_WORK=$WORK                                                 #
  HOME_ETC="${HOME_WORK}/predictability/etc"                      # Root directory for template an initialization files
  HOME_DAT="${HOME_WORK}/predictability/data/wrfrun"              # Root directory for wrf model initial and boudary conditions
  WRFMODEL_ROOT="${HOME_WORK}/wrf_model"                          # Root directory for codes and executables retrieval
  HOME_RUN=${CINECA_SCRATCH}/${SIMULATION}_${JID}                 # Root directory for run
                                                                  # Be careful: for this execution the $JID is defined in the Job script
  WRF_CODE_HOME="${WRFMODEL_ROOT}/wrf-intel/wrf-3.9.1.1/WRFV3"    # WRF model installation directory
  WPS_CODE_HOME="${WRFMODEL_ROOT}/wrf-intel/wrf-3.9.1.1/WPS"      # WPS pre-processor installation directory
  GEODATA_DIR="${WRFMODEL_ROOT}/geog/"                            # Static boundary conditions root directory 
  WRF_RUN_DIR="${HOME_RUN}/wrf_run"                               # WRF model run directory
  WPS_RUN_DIR="${HOME_RUN}/wps_run"                               # WPS pre-processor run directory
  DOMAINS_DIR="${HOME_RUN}/Domains"                               # WPS outputs directory
  #
  # EXPORT VARIABLES REQUIRED IN THE ENVIRONMENT
  #
  # NetCDF libraries first
  #
  export NETCDF=${WRFMODEL_ROOT}/lib/intel/netcdf-4.4.1.1
  export PATH=${NETCDF}/bin:${PATH}
  export LIBPATH=${NETCDF}/lib:${LIBPATH:-""}
  export LD_LIBRARY_PATH=${NETCDF}/lib:${LD_LIBRARY_PATH:-}
  #
  # zlibs, jasperlibs and png for WPS
  #
  LIB_ROOT=${WRFMODEL_ROOT}/lib/intel
  JASPERLIB=${LIB_ROOT}/jasper-1.701.0/lib
  PNGLIB=${LIB_ROOT}/libpng-1.2.12/lib
  ZLIB=${LIB_ROOT}/zlib-1.2.8/lib
  JASPERBIN=${LIB_ROOT}/jasper-1.701.0/bin
  PNGBIN=${LIB_ROOT}/libpng-1.2.12/bin
  export LD_LIBRARY_PATH=${JASPERLIB}:${PNGLIB}:${ZLIB}:${LD_LIBRARY_PATH:-}
  export PATH=${JASPERBIN}:${PNGBIN}:${PATH}
  #
  # ENVIRONMENTAL MODULES TO BE LOADED
  #
  MODULE_TO_LOAD="autoload intel/pe-xe-2018--binary intelmpi/2018--binary"
  #
  # Template namelist file for WPS
  #
  WPS_NAMELIST_TPL="${HOME_ETC}/wrf4ana_namelist.wps.template.Ophelia-TC_4ME"
  #
  # Template namelist file for WRF
  #
  WRF_NAMELIST_TPL="${HOME_ETC}/wrf4ana_namelist.wrf.template.Ophelia-TC_4ME"
#
# +--------------------------------------------------------------------------------+
# |                                                                                |
# |               VARIABLES AND PARAMETERS FOR geogrid RUN                         |
# |                                                                                |
# +--------------------------------------------------------------------------------+
#
#
# Files to be linked in the run directory and required to run geogrid
#
  GEOGRID_TABLE="${WPS_CODE_HOME}/geogrid/GEOGRID.TBL.ARW"  # Geogrid table
  GEOGRID_EXE="${WPS_CODE_HOME}/geogrid/geogrid.exe"        # geogrid executable
#
# +--------------------------------------------------------------------------------+
# |                                                                                |
# |               VARIABLES AND PARAMETERS FOR ungrib  RUN                         |
# |                                                                                |
# +--------------------------------------------------------------------------------+
#
# Directory and template file name for GRIB files identification
#
  GRIB_REPO_DIR="${WRFMODEL_ROOT}/data/ecmwf/atlantic_analysis/"          
  GRIB_FILE_TPL="${GRIB_REPO_DIR}/%YYYY%/ECMWF_ana_%YYYY%%MM%%DD%_??.GRIB"
#
# Directory where to link GRIB files required for simulation       
#
  GRIB_TEMP_DIR="${HOME_RUN}/GRIB"
#
# Files to be linked in the run directory and required to run ungrib 
#
  UNGRIB_EXE="${WPS_CODE_HOME}/ungrib/ungrib.exe"                  # ungrib  executable
  VTABLE="${WPS_CODE_HOME}/ungrib/Variable_Tables/Vtable.ECMWF"    # The Vtable to decode GRIB files
  GRIB_LINKER="${WPS_CODE_HOME}/link_grib.csh"                     # GRIB files linker and organizer
#
# +--------------------------------------------------------------------------------+
# |                                                                                |
# |               VARIABLES AND PARAMETERS FOR metgrid RUN                         |
# |                                                                                |
# +--------------------------------------------------------------------------------+
#
#
# Files to be linked in the run directory and required to run metgrid
#
  METGRID_EXE="${WPS_CODE_HOME}/metgrid/metgrid.exe"               # metgrid  executable
  MTABLE="${WPS_CODE_HOME}/metgrid/METGRID.TBL.ARW"                # The METGRID.TBL 
#
# +--------------------------------------------------------------------------------+
# |                                                                                |
# |               VARIABLES AND PARAMETERS FOR real    RUN                         |
# |                                                                                |
# +--------------------------------------------------------------------------------+
#
  # The directory in which all required files to run real.exe  are available
  REAL_DIR="${WRF_CODE_HOME}/run/"
#
# +--------------------------------------------------------------------------------+
# |                                                                                |
# |               VARIABLES AND PARAMETERS FOR wrf     RUN                         |
# |                                                                                |
# +--------------------------------------------------------------------------------+
#
  # The directory in which all required files to run wrf.exe  are available
  WRF_DIR="${WRF_CODE_HOME}/run/"
#
#
# END OF INITIALIZATON FILE
#
