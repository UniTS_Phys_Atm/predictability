#!/usr/bin/bash
#
# This program sbatches the source script several times.
# 
# Author: Enrico Maiero    
# e-mail: enricomaiero@virgilio.it
# University of Trieste
# Departmente of Physics
# ITALY
#
# Last update: Nov 28, 2018
#set -x # Let script verbose commanda (activate it for debugs)
set -e # Let script exit if a command fails
set -u # Let script exit if an unset variable is used
SOURCE=/marconi/home/userexternal/emaiero0/src/predictability/job/iss11/esegui2.job
for i in $( seq 4 10 100); do
   sed "s/MAXIMUM_INDEX=25/MAXIMUM_INDEX=$i/g" "$SOURCE" | sbatch
done
echo -e "dut a puest" 
