#!/usr/bin/bash
#
# This program calculates powers of a base given through an input file 
# with integer exponent going from 0 to a final value given by means of
# the same input file.
# The program exploits the definition of power as recursive multiplication, 
# paying attention to the meaning of base**0; it considers an error the
# value of 0 for the base.
#
# This specific version of the program writes a warning in the output file
# when the value of 10**18 is overcome by the power.
# Moreover, in this version the input file is given as an argument of the script.
# 
# Author: Enrico Maiero    
# e-mail: enricomaiero@virgilio.it
# University of Trieste
# Departmente of Physics
# ITALY
#
# Last update: Oct 24, 2018
#
# -----------------------------------------------------------------------------
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   | GENERAL SETTINGS AND DEBUG OPTIONS     | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
   #set -x # Let script verbose commanda (activate it for debugs)
   set -e # Let script exit if a command fails
   set -u # Let script exit if an unset variable is used
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   | CONSTANT AND PARAMETERS HERE BELOW     | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
   EXIT_STATUS=0          # Default exit status. 0 = everything was OK
   readonly MAXINT=$((10**18)) #near the maximum representable integer
   SOLO=0 #control variable used to write the warning message in the output file
   #echo $MAXINT #print di controllo 

   EXP=0  # Initialize the exponent
   POT=1  # Initialize the first element of the series

   # Ensuring that the first argument of the script exists and is not empty
   # If so, assigning to the variable FIN the name of th input file
   if  [[ $# -gt 0 ]] &&  [[  ! -z $1 ]];then 
      FIN=$1
   else
      echo -e "\n\tError! Input file not given. I must exit!\n"
      EXIT_STATUS=1; exit $EXIT_STATUS
   fi
   readonly FIN=$FIN #I am ensuring that FIN will not change while the program is running

   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   |         MAIN SCRIPT HERE BELOW         | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
   #   This is the input section. Exponent for BASE and BASE are loaded 
   #   from an input file.
   #
   #   The following lines has been obscured in order to use the script in a repeated way
#   echo -e "\n\tThis program calculates powers of an input base with integer exponent "
#   echo -e "\tgoing from 0 to an input final value. These values are to be written "
#   echo -e "\tin an input file, the name of which you gave me as an argument of the script. "
#   echo -e "\n\tThe input file has to present the following strcture: "
#   echo -e "\n\t\t readonly BASE=<value of the base> "
#   echo -e "\n\t\t readonly EXPM=<value of the maximum exponent>"
   OK=0 # controlling variable
   source $FIN || OK=1
   #    Exit if the input file does not exist or cannot be read
   if [[ $OK -eq 1 ]];then
               echo -e "\n\tERROR: this is not an existing or readable file\n"
               EXIT_STATUS=1; exit $EXIT_STATUS
   fi
   #base cannot be 0: in such a case exit
   if [[ $BASE -eq 0 ]];then
               echo -e "\n\tERROR: the base cannot be 0\n"
               EXIT_STATUS=1; exit $EXIT_STATUS
   fi
   #
   #Create the output file
   FOUT="output_${BASE}_${EXPM}.txt"      # Output file name
   echo "#Powers of $BASE for EXPonent going from 0 to $EXPM" > $FOUT
   echo "#BASE, EXPonent, power" >> $FOUT
   #
   #Loop, condition: is the EXPonent less or equal of EXPM?"
   while [[ $EXP -le $EXPM ]]; do
         #Calculation performed only for EXP not 0
         if [[ $EXP -ne 0 ]]; then
            OK=0
            POT=$((POT*BASE)) || OK=1
            if [[ $OK -eq 1 ]];then 
               echo -e "\n\tERROR: something went wrong in the loop\n"
               EXIT_STATUS=1; exit $EXIT_STATUS 
            fi
         fi
         #warning in case of overflow
         if [[ $POT -ge $MAXINT ]]; then
           # echo $SOLO #print di controllo
             if [[ $SOLO -eq 0 ]]; then #Ensuring that the message will appear only once
               echo "#from now on the powers are exceedding 10**18" >> $FOUT
               SOLO=1
             fi
         fi
         echo "$BASE,$EXP,$POT" >> $FOUT
         #Counter incremented
         EXP=$((EXP+1))
   done
   #
   # Some verbose before to leave
   #
   echo -e "\n\tEverything went OK.\n\tOutput file is: $FOUT\n\tbye\n"
   #
   exit $EXIT_STATUS 
   #    
   #   +----------------------------------------+  
   #   |                                        | 
   #   |           END OF THE SCRIPT            | 
   #   |                                        | 
   #   +----------------------------------------+  
   #
